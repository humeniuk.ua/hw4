const {src, dest, watch, series} = require("gulp");
const htmlmin = require("gulp-htmlmin");
const gulp_sass = require('gulp-sass')(require('sass'));
const browsersync = require("browser-sync").create();
const gulp_uglify = require("gulp-uglify");
const gulp_clean_css = require("gulp-clean-css");
const gulp_concat = require("gulp-concat");
const gulp_autoprefixer = require("gulp-autoprefixer");
const gulp_clean_dir = require("gulp-clean-dir");
const gulp_css_minify =  require("gulp-css-minify");
const gulpMinify = require("gulp-minify");
const jsmin = require("gulp-js-minify");
// import imagemin from 'gulp-imagemin';

function html (finish){
    src("./index.html")
    .pipe(htmlmin({ collapseWhitespace: true}))
    .pipe(dest("dist"));

 finish();
}

function scss(finish) {
    src("src/scss/*.scss")
    .pipe(gulp_sass({outputStyle: "compressed"}).on("error",()=>{console.log("Error in sass code!");}))
    .pipe(gulp_autoprefixer({cascade: false}))
    .pipe(gulp_concat("styles.min.css"))
    .pipe(gulp_clean_css())
    .pipe(gulp_css_minify())
    .pipe(dest("dist"));

 finish();
}

function images(finish) {
  src("src/img/*.*").pipe(dest("dist" + "/img"))

  finish();
}


function js (finish){
  src("src/js/*.js")
    .pipe(gulpMinify())
    .pipe(dest("dist"))
    finish();
}

function watcher() {
    browsersync.init({
      server: {
        baseDir: "."
      },
    });
    watch('./index.html').on('change', browsersync.reload);
    watch("src/**/*.*", series(html, scss, images, js)).on("change", browsersync.reload);
}

function buildcss(f) {
  src("src/scss/*.scss")
    .pipe(gulp_sass())
    .pipe(gulp_autoprefixer({cascade: false
    }))
    .pipe(gulp_clean_css({compatibility: 'ie8'}))
    .pipe(gulp_concat("styles.min.css"))
    .pipe(dest("dist"));
    f();
}

function buildjs (f) {
  src("src/js/*.js")
    .pipe(jsmin())
    .pipe(dest('dist'))
  f();  
}

function cleaner(f) {
  src('dist', {read: false})
  .pipe(gulp_clean_dir());
  f();
}
  
exports.cleaner = cleaner;
exports.js = js;
exports.buildcss = buildcss;
exports.buildjs = buildjs;

exports.build = series(cleaner, buildcss, buildjs, images);
exports.dev = series(scss, buildjs, watcher)
exports.default = series(html, scss, images, js, watcher);
 
  

